import { observable, action } from "mobx";

import AbstractActivity from "./AbstractActivity";
import Criterion from "./Criterion";

/**
 * Essence Standard Version 1.1, Page 97
 * A placeholder for something to be done in the software engineering endeavor
 * @extends AbstractActivity
 */
export default class ActivitySpace extends AbstractActivity {
  @observable input = [];
  @observable type = "Activity Space";

  @action.bound
  destroy() {
    this.deleted = true;
    this.owner.removeActivitySpace(this);
  }

  static fromJSON(json, alphas, kernel) {
    let space = new ActivitySpace(json.name, kernel);
    space.id = json.id;
    space.briefDescription = json.brief_description;
    space.description = json.description;
    space.concern = kernel.ownedElements.concerns.children.find(c => c.name === json.concern);
    for (let criterion of json.criterions) {
      space.criterion.push(Criterion.fromJSON(criterion, alphas, space));
    }
    return space;
  }
}
