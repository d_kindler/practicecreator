import { observable, action } from "mobx";

import LanguageElement from "./LanguageElement";

/**
 * Essence Standard Version 1.1, Page 73
 * A generic name for an Essence concept that names a collection of elements.
 * Element groups are recursive, so a group may own other groups, as well
 * as other (non-group) elements.
 * @extends LanguageElement
 */
export default class ElementGroup extends LanguageElement {
  @observable name;
  @observable briefDescription;
  @observable description;
  @observable referredElements = [];
  @observable ownedElements = [];

  constructor(name) {
    super();
    this.name = name;
  }

  @action.bound
  setName(name) {
    this.name = name;
  }

  @action.bound
  setBriefDescription(briefDescription) {
    this.briefDescription = briefDescription;
  }

  @action.bound
  setDescription(description) {
    this.description = description;
  }
}
