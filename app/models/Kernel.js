import { observable, action } from "mobx";

import sematEssenceData from "../utils/sematEssenceStandard.json";
import TreeList from "./TreeList";
import Alpha from "./Alpha";
import Checkpoint from "./Checkpoint";
import ElementGroup from "./ElementGroup";
import ActivitySpace from "./ActivitySpace";
import Concern from "./Concern";
import Competency from "./Competency";

/**
 * Essence Standard Version 1.1, Page 76
 * A kernel is a set of elements used to form a common ground for describing a software
 * engineering endeavor. A kernel is an element group that names the basic concepts
 * (i.e., alphas, activity spaces and competencies) for a domain (e.g., Software Engineering).
 * @extends ElementGroup
 */
export default class Kernel extends ElementGroup {
  @observable store;
  @observable consistencyRules;
  @observable type = "Kernel";

  constructor(name, store) {
    super(name);
    this.store = store;
    // Concerns
    this.ownedElements.concerns = new TreeList("Concern List");
    for (const concern of sematEssenceData.kernel.concerns) {
      this.ownedElements.concerns.push(Concern.fromJSON(concern, this));
    }
    // Alphas and Alpha States
    this.ownedElements.alphas = new TreeList("Alpha List");
    for (const alpha of sematEssenceData.kernel.alphas) {
      this.ownedElements.alphas.children.push(
        Alpha.fromJSON(alpha, this.ownedElements.concerns.children, this)
      );
    }
    // Checkpoints
    for (const checkpoint of sematEssenceData.kernel.checkpoints.reverse()) {
      for (const alpha of this.ownedElements.alphas.children) {
        let found = false;
        for (const state of alpha.states) {
          if (checkpoint.assessable_id === state.id) {
            state.checkpoints.push(Checkpoint.fromJSON(checkpoint, state));
            found = true;
            break;
          }
        }
        if (found) {
          break;
        }
      }
    }
    // Activity Spaces and Criteria
    this.ownedElements.activitySpaces = new TreeList("Activity Space List");
    for (const activitySpace of sematEssenceData.kernel.activity_spaces) {
      this.ownedElements.activitySpaces.push(
        ActivitySpace.fromJSON(activitySpace, this.ownedElements.alphas.children, this)
      );
    }
    // Competencies
    this.ownedElements.competencies = new TreeList("Competency List");
    for (const competency of sematEssenceData.kernel.competencies) {
      this.ownedElements.competencies.push(Competency.fromJSON(competency, this));
    }
  }

  @action.bound
  setConsistencyRules(consistencyRules) {
    this.consistencyRules = consistencyRules;
  }

  @action.bound
  addAlpha(name) {
    let alpha = new Alpha(name, this);
    this.ownedElements.alphas.children.push(alpha);
  }

  @action.bound
  removeAlpha(alpha) {
    this.ownedElements.alphas.children.remove(alpha);
  }

  @action.bound
  addActivitySpace(name) {
    let activitySpace = new ActivitySpace(name, this);
    this.ownedElements.activitySpaces.children.push(activitySpace);
  }

  @action.bound
  removeActivitySpace(activitySpace) {
    this.ownedElements.activitySpaces.children.remove(activitySpace);
  }

  @action.bound
  addCompetency(name) {
    let competency = new Competency(name, this);
    this.ownedElements.competencies.children.push(competency);
  }

  @action.bound
  removeCompetency(competency) {
    this.ownedElements.competencies.children.remove(competency);
  }

  @action.bound
  addConcern(name) {
    let concern = new Concern(name, this);
    this.ownedElements.concerns.children.push(concern);
  }

  @action.bound
  removeConcern(concern) {
    this.ownedElements.concerns.children.remove(concern);
  }

  @action.bound
  destroy() {
    this.deleted = true;
    this.store.children.remove(this);
  }
}
