import { observable } from "mobx";

import TreeViewElement from "./TreeViewElement";

/**
 * Essence Standard Version 1.1, Page 77
 * Abstract superclass for an Essence concept.
 * @extends TreeViewElement
 */
export default class LanguageElement extends TreeViewElement {
  @observable id;
  @observable owner;

  constructor(owner) {
    super();
    this.owner = owner;
  }

  checkInvariants() {
    return true;
  }
}
