import { observable, action, computed } from "mobx";

import Checkpoint from "./Checkpoint";
import LanguageElement from "./LanguageElement";

/**
 * Essence Standard Version 1.1, Page 89
 * A specification of the state of progress of an alpha.
 * The state of an alpha is determined by evaluating checklist items.
 * @extends LanguageElement
 */
export default class State extends LanguageElement {
  @observable name;
  @observable description;
  @observable checkpoints = [];
  @observable type = "State";

  constructor(name, owner) {
    super(owner);
    this.name = name;
  }

  @computed
  get path() {
    return this.owner.name + " / " + this.name;
  }

  @action.bound
  setName(name) {
    this.name = name;
  }

  @action.bound
  setDescription(description) {
    this.description = description;
  }

  @action.bound
  addCheckpoint(name) {
    let checkpoint = new Checkpoint(name, this);
    this.checkpoints.push(checkpoint);
  }

  @action.bound
  removeCheckpoint(checkpoint) {
    this.checkpoints.remove(checkpoint);
  }

  @action.bound
  destroy() {
    this.deleted = true;
    this.owner.removeState(this);
  }

  static fromJSON(json, alpha) {
    let alphaState = new State(json.name, alpha);
    alphaState.id = json.id;
    alphaState.description = json.description;
    alphaState.based_on_id = json.based_on_id;
    return alphaState;
  }

  checkInvariants() {
    super.checkInvariants();
    this.checkCheckpointNames();
  }

  // TODO: Test
  /**
   * -- All checkpoints of a state must have different names
   * self.checkListItem->forAll(i1, i2 | i1 <> i2 implies i1.name <> i2.name)
   */
  checkCheckpointNames() {
    this.checkpoints.map(item => item.name)
      .filter((value, index, array) => array.indexOf(value) !== index);
    return true;
  }
}
