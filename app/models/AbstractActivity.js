import { observable, action } from "mobx";

import BasicElement from "./BasicElement";
import Criterion from "./Criterion";

/**
 * Essence Standard Version 1.1, Page 93
 * A placeholder for something to be done in the software engineering endeavor
 * @extends BasicElement
 */
export default class AbstractActivity extends BasicElement {
  @observable criterion = [];
  @observable type = "Abstract Activity";

  @action.bound
  addCriterion(name) {
    let criterion = new Criterion(name, this);
    this.criterion.push(criterion);
  }

  @action.bound
  removeCriterion(criterion) {
    this.criterion.remove(criterion);
  }
}
