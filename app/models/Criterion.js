import { observable, action, computed } from "mobx";

import LanguageElement from "./LanguageElement";

const criterionTypeEnum = ["Completion", "Entry"];

/**
 * Essence Standard Version 1.1, Page 98
 * A condition that can be tested as true or false that contributes to the determination of
 * whether an activity or an activity space may be entered or is complete.
 * A criterion is expressed in terms of the state of an alpha or the level
 * of detail of a work product.
 * The abstract Criterion must be specialized by EntryCriterion or Completion Criterion.
 * @extends LanguageElement
 */
export default class Criterion extends LanguageElement {
  @observable description;
  @observable state;
  @observable levelOfDetail;
  @observable criterionType = criterionTypeEnum[0];
  @observable type = "Criterion";

  constructor(description, owner) {
    super(owner);
    this.description = description;
  }

  @action.bound
  setDescription(description) {
    this.description = description;
  }

  @action.bound
  setState(state) {
    this.state = state;
    this.levelOfDetail = null;
  }

  @action.bound
  setLevelOfDetail(lod) {
    this.state = null;
    this.levelOfDetail = lod;
  }

  @action.bound
  setCriterionType(type) {
    if (criterionTypeEnum.includes(type)) {
      this.criterionType = type;
    } else {
      alert("Invalid type");
    }
  }

  @action.bound
  destroy() {
    this.deleted = true;
    this.owner.removeCriterion(this);
  }

  @computed
  get allStates() {
    let element = this;
    let alphas = [];
    while (true) {
      if (element.type === "Kernel") {
        for (const alpha of element.ownedElements.alphas.children) {
          alphas.push(alpha);
        }
        break;
      } else if (element.type === "Practice") {
        for (const alpha of element.ownedElements.alphaList.children) {
          alphas.push(alpha);
        }
        element = element.kernel;
      } else {
        element = element.owner;
      }
    }
    let states = [];
    for (const alpha of alphas) {
      for (const state of alpha.states) {
        states.push(state);
      }
    }
    return states;
  }

  static fromJSON(json, alphas, space) {
    let criterion = new Criterion(json.description, space);
    criterion.id = json.id;
    let alpha = alphas.find(a => a.based_on_id === json.alpha_id);
    if (alpha) {
      criterion.state = alpha.states.find(s => s.based_on_id === json.alpha_state_id);
    }
    return criterion;
  }
}
