import { observable, action, reaction, toJS } from "mobx";
import { isEqual } from "lodash";

import BasicElement from "./BasicElement";
import CompetencyLevel from "./CompetencyLevel";

/**
 * Essence Standard Version 1.1, Page 100
 * A competency encompasses the abilities, capabilities, attainments,
 * knowledge, and skills necessary to do a certain kind of work.
 * It is assumed that for each team member a level of competency
 * for each individual competency can be named or determined
 * @extends BasicElement
 */
export default class Competency extends BasicElement {
  @observable possibleLevel = [];
  @observable type = "Competency";

  @action.bound
  addLevel(name) {
    let level = new CompetencyLevel(name, this);
    this.possibleLevel.push(level);
    this.sortLevels();
  }

  @action.bound
  sortLevels() {
    let sortedLevels = this.possibleLevel.sort((a, b) => b.level - a.level);
    this.possibleLevel.replace(sortedLevels);
  }

  @action.bound
  removeLevel(level) {
    this.possibleLevel.remove(level);
  }

  @action.bound
  destroy() {
    this.deleted = true;
    this.owner.removeCompetency(this);
  }

  static fromJSON(json, kernel) {
    let competency = new Competency(json.name, kernel);
    competency.id = json.id;
    competency.briefDescription = json.brief_description;
    competency.description = json.description;
    competency.concern = kernel.ownedElements.concerns.children.find(c => c.name === json.concern);
    for (let level of json.levels) {
      competency.possibleLevel.push(CompetencyLevel.fromJSON(level, competency));
    }
    return competency;
  }
}
