import { observable, action } from "mobx";

import ElementGroup from "./ElementGroup";
import Activity from "./Activity";
import WorkProduct from "./WorkProduct";
import TreeList from "./TreeList";
import Alpha from "./Alpha";

/**
 * Essence Standard Version 1.1, Page 81
 * A practice is a repeatable approach to doing something with a specific objective in mind.
 * A practice describes how to handle a specific aspect of a software engineering endeavor,
 * including the descriptions of all relevant elements necessary to express the desired work
 * guidance that is required to achieve the purpose of the practice. A practice can be defined as a
 * composition of other practices.
 * @extends ElementGroup
 */
export default class Practice extends ElementGroup {
  @observable kernel;
  @observable consistencyRules;
  @observable objective;
  @observable measures;
  @observable entry;
  @observable result;
  @observable type = "Practice";

  constructor(name, owner) {
    super(name, owner);
    this.ownedElements.activityList = new TreeList("Activity List");
    this.ownedElements.workProductList = new TreeList("Work Product List");
    this.ownedElements.alphaList = new TreeList("Alpha List");
  }

  @action.bound
  setKernel(kernel) {
    if (this.kernel) {
      alert("Existing Kernel cannot be replaced!");
    } else {
      this.kernel = kernel;
    }
  }

  @action.bound
  setConsistencyRules(consistencyRules) {
    this.consistencyRules = consistencyRules;
  }

  @action.bound
  setObjective(objective) {
    this.objective = objective;
  }

  @action.bound
  setMeasures(measures) {
    this.measures = measures;
  }

  @action.bound
  setEntry(entry) {
    this.entry = entry;
  }

  @action.bound
  setResult(result) {
    this.result = result;
  }

  @action.bound
  addActivity(name) {
    let activity = new Activity(name, this);
    this.ownedElements.activityList.children.push(activity);
  }

  @action.bound
  removeActivity(activity) {
    this.ownedElements.activityList.children.remove(activity);
  }

  @action.bound
  addWorkProduct(name) {
    let workProduct = new WorkProduct(name, this);
    this.ownedElements.workProductList.children.push(workProduct);
  }

  @action.bound
  removeWorkProduct(workProduct) {
    this.ownedElements.workProductList.children.remove(workProduct);
  }

  @action.bound
  addAlpha(name) {
    let alpha = new Alpha(name, this);
    this.ownedElements.alphaList.children.push(alpha);
  }

  @action.bound
  removeAlpha(alpha) {
    this.ownedElements.alphaList.children.remove(alpha);
  }

  @action.bound
  destroy() {
    this.deleted = true;
    this.owner.children.remove(this);
  }
}
