import { observable, action, computed } from "mobx";

import AbstractActivity from "./AbstractActivity";
import TreeList from "./TreeList";
import Approach from "./Approach";
import Action from "./Action";

/**
 * Essence Standard Version 1.1, Page 95
 * An Activity defines one or more approaches for carrying out some work to be performed and can
 * recommend actions on alphas and/or work products in order to perform this work.
 * @extends AbstractActivity
 */
export default class Activity extends AbstractActivity {
  @observable approach = new TreeList("Approach List");
  @observable action = new TreeList("Action List");
  @observable requiredCompetencyLevel = [];
  @observable activitySpaces = [];
  @observable type = "Activity";

  constructor(name, owner) {
    super(name, owner);
    this.addApproach("Approach 1");
  }

  @action.bound
  setActivitySpaces(activitySpaces) {
    this.activitySpaces = [];
    for (const activitySpacesElement of activitySpaces) {
      this.activitySpaces.push(activitySpacesElement.realValue);
    }
  }

  @action.bound
  setRequiredCompetencyLevel(requiredCompetencyLevel) {
    this.requiredCompetencyLevel = [];
    for (const requiredCompetencyLevelElement of requiredCompetencyLevel) {
      this.requiredCompetencyLevel.push(requiredCompetencyLevelElement.realValue);
    }
  }

  @action.bound
  addApproach(name) {
    let approach = new Approach(name, this);
    this.approach.children.push(approach);
  }

  @action.bound
  removeApproach(approach) {
    this.approach.children.remove(approach);
  }

  @action.bound
  addAction() {
    let activityAction = new Action(this);
    this.action.children.push(activityAction);
  }

  @action.bound
  removeAction(activityAction) {
    this.action.children.remove(activityAction);
  }

  @computed
  get allCompetencyLevels() {
    let levels = [];
    for (const competency of this.owner.kernel.ownedElements.competencies.children) {
      for (const level of competency.possibleLevel) {
        levels.push(level);
      }
    }
    return levels;
  }

  @action.bound
  destroy() {
    this.deleted = true;
    this.owner.removeActivity(this);
  }
}
