import { observable, action } from "mobx";

/**
 * Contains the Tree View State to set an Element active or toggled
 */
export default class TreeViewElement {
  @observable active;
  @observable toggled;
  @observable deleted = false;

  @action.bound
  setActive(active) {
    this.active = active;
  }

  @action.bound
  setToggled(toggled) {
    this.toggled = toggled;
  }
}
