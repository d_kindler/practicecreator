import { observable, action } from "mobx";

import LanguageElement from "./LanguageElement";
/**
 * Essence Standard Version 1.1, Page 89
 * A specification of the amount of detail or range of content in a work product.
 * The level of detail of a work product is determined by evaluating checklist items.
 * @extends LanguageElement
 */
export default class LevelOfDetail extends LanguageElement {
  @observable name;
  @observable isSufficientLevel;
  @observable description;
  @observable checkListItem;
  @observable type = "Level Of Detail";

  constructor(name, owner) {
    super();
    this.name = name;
    this.owner = owner;
    this.isSufficientLevel = false;
  }

  @action.bound
  setName(name) {
    this.name = name;
  }

  @action.bound
  setDescription(description) {
    this.description = description;
  }

  @action.bound
  setIsSufficientLevel(isSufficientLevel) {
    this.isSufficientLevel = isSufficientLevel;
  }

  @action.bound
  destroy() {
    this.deleted = true;
    this.owner.removeLevelOfDetail(this);
  }
}
