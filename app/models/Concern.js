import { observable, action } from "mobx";

import LanguageElement from "./LanguageElement";

/**
 * Not part of the Essence Standard (to the best of my knowledge)
 * @extends LanguageElement
 */
export default class Concern extends LanguageElement {
  @observable name;
  @observable briefDescription;
  @observable description;
  @observable color;
  @observable type = "Concern";

  constructor(name, owner) {
    super(owner);
    this.name = name;
  }

  @action.bound
  setName(name) {
    this.name = name;
  }

  @action.bound
  setBriefDescription(briefDescription) {
    this.briefDescription = briefDescription;
  }

  @action.bound
  setDescription(description) {
    this.description = description;
  }

  @action.bound
  setColor(color) {
    this.color = color;
  }

  @action.bound
  destroy() {
    this.deleted = true;
    this.owner.removeConcern(this);
  }

  static fromJSON(json, kernel) {
    let concern = new Concern(json.name, kernel);
    concern.id = json.id;
    concern.briefDescription = json.briefDescription;
    concern.description = json.description;
    concern.color = json.color;
    return concern;
  }
}
