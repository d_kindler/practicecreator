import { observable, action } from "mobx";

import LanguageElement from "./LanguageElement";

/**
 * Essence Standard Version 1.1, Page 72
 * A condition that can be tested as true or false that contributes to the determination of
 * whether a state (of an alpha) or a level of detail (of a work product) or a competency
 * level has been attained.
 * @extends LanguageElement
 */
export default class Checkpoint extends LanguageElement {
  @observable name;
  @observable description;
  @observable shortDescription;
  @observable type = "Checkpoint";

  constructor(name, owner) {
    super(owner);
    this.name = name;
  }

  @action.bound
  setName(name) {
    this.name = name;
  }

  @action.bound
  setShortDescription(shortDescription) {
    this.shortDescription = shortDescription;
  }

  @action.bound
  setDescription(description) {
    this.description = description;
  }

  @action.bound
  destroy() {
    this.deleted = true;
    this.owner.removeCheckpoint(this);
  }

  static fromJSON(json, state) {
    let checkpoint = new Checkpoint(json.name, state);
    checkpoint.id = json.id;
    checkpoint.shortDescription = json.short_description;
    checkpoint.description = json.description;
    return checkpoint;
  }
}
