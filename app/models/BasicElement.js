import { observable, action } from "mobx";

import LanguageElement from "./LanguageElement";
/**
 * Essence Standard Version 1.1, Page 71
 * Abstract superclass for all main concepts in Essence other than Element groups
 * @extends LanguageElement
 */
export default class BasicElement extends LanguageElement {
  @observable name;
  @observable briefDescription;
  @observable description;
  @observable concern;

  constructor(name, owner) {
    super(owner);
    this.name = name;
  }

  @action.bound
  setName(name) {
    this.name = name;
  }

  @action.bound
  setBriefDescription(briefDescription) {
    this.briefDescription = briefDescription;
  }

  @action.bound
  setDescription(description) {
    this.description = description;
  }

  @action.bound
  setConcern(concern) {
    this.concern = concern;
  }
}
