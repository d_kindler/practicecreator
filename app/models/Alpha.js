import { observable, action, computed } from "mobx";
import { createViewModel } from "mobx-utils";

import State from "./State";
import BasicElement from "./BasicElement";

/**
 * Essence Standard Version 1.1, Page 86
 * An essential element that is relevant to an assessment of the progress and health of a
 * software engineering endeavor. An alpha represents and holds the state of some
 * element, aspect, or abstraction in an endeavor that has a discernible state and knowledge of
 * whose state is required to understand the state of progress and/or health of the endeavor.
 * The instances of alphas in an endeavor form acyclic graphs.
 * These graphs show how the states of lower level, more granular instances, contribute to and
 * drive the states of the higher level, more abstract, alphas.
 * @extends BasicElement
 */
export default class Alpha extends BasicElement {
  @observable states = [];
  @observable type = "Alpha";

  // https://github.com/mobxjs/mobx-utils#createviewmodel
  @action.bound
  addState(name) {
    // TODO: generate ID
    const viewModel = createViewModel(this);
    let state = new State(name, this);
    viewModel.states = [...this.states, state];
    let duplicates = Alpha.checkStateNames(viewModel);
    if (duplicates.length === 0) {
      viewModel.submit();
    } else {
      alert("Cannot add States with duplicate names: " + duplicates.join());
    }
  }

  @action.bound
  removeState(state) {
    this.states.remove(state);
  }

  @action.bound
  destroy() {
    this.deleted = true;
    this.owner.removeAlpha(this);
  }

  @computed
  get allConcerns() {
    switch (this.owner.type) {
      case "Kernel":
        return this.owner.ownedElements.concerns.children;
      case "Practice":
        return this.owner.kernel.ownedElements.concerns.children;
      default:
        break;
    }
  }

  static fromJSON(json, concerns, kernel) {
    let alpha = new Alpha(json.name, kernel);
    alpha.id = json.id;
    alpha.briefDescription = json.brief_description;
    alpha.description = json.description;
    alpha.concern = concerns.find(c => c.name === json.concern);
    alpha.based_on_id = json.based_on_id;
    for (let alphaStateJson of json.states) {
      alpha.states.push(State.fromJSON(alphaStateJson, alpha));
    }
    return alpha;
  }

  checkInvariants() {
    super.checkInvariants();
    this.checkStateNames();
  }

  /**
   * -- All states of an alpha must have different names.
   * self.states->forAll(s1, s2 | s1 <> s2 implies s1.name <> s2.name)
   */
  static checkStateNames(alpha) {
    return alpha.states
      .map(item => item.name)
      .filter((value, index, array) => array.indexOf(value) !== index);
  }
}
