import { observable, action, computed } from "mobx";

import BasicElement from "./BasicElement";
import LevelOfDetail from "./LevelOfDetail";

/**
 * Essence Standard Version 1.1, Page 90
 * A work product is an artifact of value and relevance for a software engineering endeavor.
 * A work product may be a document or a piece of software, but also other created entities such as:
 * -Creation of a test environment
 * -Delivery of a training course
 */
export default class WorkProduct extends BasicElement {
  @observable levelOfDetail = [];
  @observable alphas = [];
  @observable type = "Work Product";

  constructor(name, owner) {
    super(name);
    this.owner = owner;
  }

  @action.bound
  addLevelOfDetail(name) {
    let levelOfDetail = new LevelOfDetail(name, this);
    this.levelOfDetail.push(levelOfDetail);
  }

  @action.bound
  removeLevelOfDetail(levelOfDetail) {
    this.levelOfDetail.remove(levelOfDetail);
  }

  @action.bound
  setAlphas(alphas) {
    this.alphas = [];
    for (const alphaElement of alphas) {
      this.alphas.push(alphaElement.realValue);
    }
  }

  @computed
  get allAlphas() {
    let alphas = [];
    for (const alpha of this.owner.ownedElements.alphaList.children) {
      alphas.push(alpha);
    }
    for (const alpha of this.owner.kernel.ownedElements.alphas.children) {
      alphas.push(alpha);
    }
    return alphas;
  }

  @action.bound
  destroy() {
    this.deleted = true;
    this.owner.removeWorkProduct(this);
  }
}
