import { observable, action } from "mobx";

import LanguageElement from "./LanguageElement";

/**
 * Essence Standard Version 1.1, Page 97
 * An Approach defines one way to accomplish some work.
 * An approach is specified in the context of a specific activity.
 * @extends LanguageElement
 */
export default class Approach extends LanguageElement {
  @observable name;
  @observable description;
  @observable type = "Approach";

  constructor(name, owner) {
    super(owner);
    this.name = name;
  }

  @action.bound
  setName(name) {
    this.name = name;
  }

  @action.bound
  setDescription(description) {
    this.description = description;
  }

  @action.bound
  destroy() {
    this.deleted = true;
    this.owner.removeApproach(this);
  }
}
