import { observable, action, computed } from "mobx";

import LanguageElement from "./LanguageElement";

/**
 * Essence Standard Version 1.1, Page 101
 * A competency level defines a level of how competent or able a team member is with
 * respect to the abilities, capabilities, attainments, knowledge, or skills defined
 * by the respective competency.
 * @extends LanguageElement
 */
export default class CompetencyLevel extends LanguageElement {
  @observable name;
  @observable briefDescription;
  @observable level;
  @observable type = "Competency Level";

  constructor(name, owner) {
    super(owner);
    this.name = name;
  }

  @action.bound
  setName(name) {
    this.name = name;
  }

  @action.bound
  setBriefDescription(briefDescription) {
    this.briefDescription = briefDescription;
  }

  @action.bound
  setLevel(level) {
    this.level = level;
    this.owner.sortLevels();
  }

  @computed
  get path() {
    return this.owner.name + " / " + this.name;
  }

  @action.bound
  destroy() {
    this.owner.removeLevel(this);
  }

  static fromJSON(json, competency) {
    let level = new CompetencyLevel(json.name, competency);
    level.id = json.id;
    level.briefDescription = json.brief_description;
    level.level = json.level;
    return level;
  }
}
