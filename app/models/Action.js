import { observable, action } from "mobx";

import LanguageElement from "./LanguageElement";

const kindEnum = ["create", "read", "update", "delete"];

/**
 * Essence Standard Version 1.1, Page 94
 * An operation performed by an activity on a particular work product.
 * @extends LanguageElement
 */
export default class Action extends LanguageElement {
  @observable kind = kindEnum[0];
  @observable alpha = [];
  @observable workProduct = [];
  @observable type = "Action";

  @action.bound
  setKind(kind) {
    if (kindEnum.includes(kind)) {
      this.kind = kind;
    } else {
      alert("Invalid kind");
    }
  }

  @action.bound
  addAlpha(alpha) {
    this.alpha.push(alpha);
  }

  @action.bound
  removeAlpha(alpha) {
    this.alpha.remove(alpha);
  }

  @action.bound
  addWorkProduct(workProduct) {
    this.workProduct.push(workProduct);
  }

  @action.bound
  removeWorkProduct(workProduct) {
    this.workProduct.remove(workProduct);
  }

  @action.bound
  destroy() {
    this.deleted = true;
    this.owner.removeAction(this);
  }
}
