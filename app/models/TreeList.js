import { observable, action } from "mobx";

import LanguageElement from "./LanguageElement";

export default class TreeList extends LanguageElement {
  @observable name;
  @observable children = [];
  @observable type;

  constructor(name) {
    super();
    this.name = name;
    this.type = name;
  }

  @action.bound
  push(element) {
    this.children.push(element);
  }
}
