import { remote } from "electron";
import fs from "fs";
import modal from "electron-modal";

const { Menu, MenuItem, dialog } = remote;

function createElement(element) {
  return new Promise(resolve =>
    modal
      .open(
        `${__dirname}\\components\\NewElement\\NewElement.html`,
        { width: 400, height: 300 },
        { title: "Create new " + element }
      )
      .then(instance => instance.on("save", name => resolve(name)))
  );
}

function exportElement(element) {
  const options = {
    title: "Export " + element.name,
    filters: [{ name: "JSON", extensions: ["json"] }]
  };
  dialog.showSaveDialog(options, filePath => {
    if (filePath === undefined) {
      return;
    }
    alert("Diese Funktion wurde noch nicht vollständig implementiert");
    fs.writeFile(filePath, "Diese Funktion wurde noch nicht vollständig implementiert", err => {
      if (!err) {
        dialog.showMessageBox({
          message: element.name + " has been successfully exported!",
          buttons: ["OK"]
        });
      } else {
        dialog.showErrorBox("File save error", err.message);
      }
    });
  });
}

export function getContextMenu(element) {
  let menu = new Menu();
  switch (element.type) {
    case "Practice":
    case "Activity":
    case "Approach":
    case "Work Product":
    case "Level Of Detail":
    case "Kernel":
    case "Alpha":
    case "State":
    case "Checkpoint":
    case "Activity Space":
    case "Criterion":
    case "Competency":
    case "Competency Level":
    case "Concern":
      menu.append(
        new MenuItem({
          label: "Delete",
          click: () => element.destroy()
        })
      );
      break;
    default:
      break;
  }
  switch (element.type) {
    case "Practices":
      menu.append(
        new MenuItem({
          label: "Create new Practice",
          click: () => createElement("Practice").then(name => element.addPractice(name))
        })
      );
      break;
    case "Practice":
      menu.append(
        new MenuItem({
          label: "Create new Activity",
          click: () => createElement("Activity").then(name => element.addActivity(name))
        })
      );
      menu.append(
        new MenuItem({
          label: "Create new Work Product",
          click: () => createElement("Work Product").then(name => element.addWorkProduct(name))
        })
      );
      menu.append(
        new MenuItem({
          label: "Create new Alpha",
          click: () => createElement("Alpha").then(name => element.addAlpha(name))
        })
      );
      menu.append(
        new MenuItem({
          label: "Export Practice",
          click: () => exportElement(element)
        })
      );
      break;
    case "Activity":
      menu.append(
        new MenuItem({
          label: "Create new Approach",
          click: () => createElement("Approach").then(name => element.addApproach(name))
        })
      );
      menu.append(
        new MenuItem({
          label: "Create new Action",
          click: () => element.addAction()
        })
      );
      menu.append(
        new MenuItem({
          label: "Create new Criterion",
          click: () => createElement("Criterion").then(name => element.addCriterion(name))
        })
      );
      break;
    case "Work Product":
      menu.append(
        new MenuItem({
          label: "Create new Level Of Detail",
          click: () => createElement("Level Of Detail").then(name => element.addLevelOfDetail(name))
        })
      );
      break;
    case "Kernels":
      menu.append(
        new MenuItem({
          label: "Create new Kernel",
          click: () => createElement("Kernel").then(name => element.addStandardKernel(name))
        })
      );
      break;
    case "Kernel":
      menu.append(
        new MenuItem({
          label: "Create new Alpha",
          click: () => createElement("Alpha").then(name => element.addAlpha(name))
        })
      );
      menu.append(
        new MenuItem({
          label: "Create new Activity Space",
          click: () => createElement("Activity Space").then(name => element.addActivitySpace(name))
        })
      );
      menu.append(
        new MenuItem({
          label: "Create new Competency",
          click: () => createElement("Competency").then(name => element.addCompetency(name))
        })
      );
      menu.append(
        new MenuItem({
          label: "Create new Concern",
          click: () => createElement("Concern").then(name => element.addConcern(name))
        })
      );
      menu.append(
        new MenuItem({
          label: "Export Kernel",
          click: () => exportElement(element)
        })
      );
      break;
    case "Alpha":
      menu.append(
        new MenuItem({
          label: "Create new Alpha State",
          click: () => createElement("Alpha State").then(name => element.addState(name))
        })
      );
      break;
    case "State":
      menu.append(
        new MenuItem({
          label: "Create new Checkpoint",
          click: () => createElement("Checkpoint").then(name => element.addCheckpoint(name))
        })
      );
      break;
    case "Activity Space":
      menu.append(
        new MenuItem({
          label: "Create new Criterion",
          click: () => createElement("Criterion").then(name => element.addCriterion(name))
        })
      );
      break;
    case "Competency":
      menu.append(
        new MenuItem({
          label: "Create new Competency Level",
          click: () => createElement("Competency Level").then(name => element.addLevel(name))
        })
      );
      break;
    default:
      break;
  }
  return menu;
}
