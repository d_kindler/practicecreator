import pick from "lodash.pick";
import TreeList from "../models/TreeList";
import Competency from "../models/Competency";
import CompetencyLevel from "../models/CompetencyLevel";

/**
 * Takes an element and prepares it to be used as an Tree View element.
 * @param {*} element Element to be prepared
 * @param {*} noChildren true if the element is not supposed to have children
 */
function prepareTreeElement(element, noChildren) {
  let preparedElement = pick(element, ["name", "active", "toggled"]);
  preparedElement.element = element;
  if (!noChildren) {
    preparedElement.children = [];
  }
  return preparedElement;
}

/**
 * Takes practices and and prepares them to be displayed by react-treebeard TreeList
 * @param {*} practices practices to be displayed
 */
export function generateTree(store) {
  let tree = [];
  let practiceList = prepareTreeElement(store.practicesStore);
  // Practices
  for (const practice of store.practicesStore.children) {
    if (!practice.kernel) {
      practiceList.children.push(prepareTreeElement(practice, true));
      continue;
    }
    let tmpPractice = prepareTreeElement(practice);
    // Activities
    let activities = prepareTreeElement(practice.ownedElements.activityList);
    for (const activity of practice.ownedElements.activityList.children) {
      let tmpActivity = prepareTreeElement(activity);
      // Approaches
      let tmpApproaches = prepareTreeElement(activity.approach);
      for (const approach of activity.approach.children) {
        let tmpApproach = prepareTreeElement(approach, true);
        tmpApproaches.children.push(tmpApproach);
      }
      tmpActivity.children.push(tmpApproaches);
      // Actions
      let tmpActions = prepareTreeElement(activity.action);
      for (const action of activity.action.children) {
        let tmpAction = prepareTreeElement(action, true);
        tmpAction.name = action.kind;
        tmpActions.children.push(tmpAction);
      }
      tmpActivity.children.push(tmpActions);
      // Criterion
      for (const criterion of activity.criterion) {
        let tmpCriterion = prepareTreeElement(criterion, true);
        tmpCriterion.name = criterion.description;
        tmpActivity.children.push(tmpCriterion);
      }
      activities.children.push(tmpActivity);
    }
    tmpPractice.children.push(activities);
    // Work Products
    let workProducts = prepareTreeElement(practice.ownedElements.workProductList);
    for (const workProduct of practice.ownedElements.workProductList.children) {
      let tmpWorkProduct = prepareTreeElement(workProduct);
      for (const levelOfDetail of workProduct.levelOfDetail) {
        let tmpLevelOfDetail = prepareTreeElement(levelOfDetail, true);
        tmpWorkProduct.children.push(tmpLevelOfDetail);
      }
      workProducts.children.push(tmpWorkProduct);
    }
    tmpPractice.children.push(workProducts);
    // Alphas
    let alphas = prepareTreeElement(practice.ownedElements.alphaList);
    for (const alpha of practice.ownedElements.alphaList.children) {
      let tmpAlpha = prepareTreeElement(alpha);
      for (const alphaState of alpha.states) {
        let tmpAlphaState = prepareTreeElement(alphaState);
        for (const checkpoint of alphaState.checkpoints) {
          let tmpCheckpoint = prepareTreeElement(checkpoint, true);
          tmpAlphaState.children.push(tmpCheckpoint);
        }
        tmpAlpha.children.push(tmpAlphaState);
      }
      alphas.children.push(tmpAlpha);
    }
    tmpPractice.children.push(alphas);
    practiceList.children.push(tmpPractice);
  }
  tree.push(practiceList);
  // Kernels
  let kernelList = prepareTreeElement(store.kernelStore);
  for (const kernel of store.kernelStore.children) {
    let tmpKernel = prepareTreeElement(kernel);
    // Alphas
    let alphaList = prepareTreeElement(kernel.ownedElements.alphas);
    for (const alpha of kernel.ownedElements.alphas.children) {
      let tmpAlpha = prepareTreeElement(alpha);
      for (const alphaState of alpha.states) {
        let tmpAlphaState = prepareTreeElement(alphaState);
        for (const checkpoint of alphaState.checkpoints) {
          let tmpCheckpoint = prepareTreeElement(checkpoint, true);
          tmpAlphaState.children.push(tmpCheckpoint);
        }
        tmpAlpha.children.push(tmpAlphaState);
      }
      alphaList.children.push(tmpAlpha);
    }
    tmpKernel.children.push(alphaList);
    // Activity Spaces
    let spaces = prepareTreeElement(kernel.ownedElements.activitySpaces);
    for (const space of kernel.ownedElements.activitySpaces.children) {
      let tmpSpace = prepareTreeElement(space);
      // Criterions
      for (const criterion of space.criterion) {
        let tmpCriterion = prepareTreeElement(criterion, true);
        tmpCriterion.name = criterion.description;
        tmpSpace.children.push(tmpCriterion);
      }
      spaces.children.push(tmpSpace);
    }
    tmpKernel.children.push(spaces);
    // Competencies
    let competencies = prepareTreeElement(kernel.ownedElements.competencies);
    for (const competency of kernel.ownedElements.competencies.children) {
      let tmpCompetency = prepareTreeElement(competency);
      for (const level of competency.possibleLevel) {
        let tmpLevel = prepareTreeElement(level, true);
        tmpCompetency.children.push(tmpLevel);
      }
      competencies.children.push(tmpCompetency);
    }
    tmpKernel.children.push(competencies);
    // Concerns
    let concerns = prepareTreeElement(kernel.ownedElements.concerns);
    for (const concern of kernel.ownedElements.concerns.children) {
      let tmpConcern = prepareTreeElement(concern, true);
      concerns.children.push(tmpConcern);
    }
    tmpKernel.children.push(concerns);
    kernelList.children.push(tmpKernel);
  }
  tree.push(kernelList);
  return tree;
}
