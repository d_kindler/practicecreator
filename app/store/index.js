import { action } from "mobx";
import Practice from "../models/Practice";
import Kernel from "../models/Kernel";
import TreeList from "../models/TreeList";
import WorkProduct from "../models/WorkProduct";
import Activity from "../models/Activity";
import Concern from "../models/Concern";

class Store {
  constructor() {
    this.kernelStore = new KernelStore(this);
    this.practicesStore = new PracticesStore(this);
  }
}

class PracticesStore extends TreeList {
  constructor(store) {
    super("Practices");
    this.store = store;
    // TODO: Only for development
    let scrumPractice = new Practice("Scrum", this);
    scrumPractice.setKernel(this.store.kernelStore.children[0]);
    let concerns = scrumPractice.kernel.ownedElements.concerns.children;
    let concern1 = new Concern("Concern 1", scrumPractice.kernel);
    let concern2 = new Concern("Concern 2", scrumPractice.kernel);
    concern1.color = "#79504a";
    concern2.color = "#68a429";
    concerns.push(concern1);
    concerns.push(concern2);
    let spaces = scrumPractice.kernel.ownedElements.activitySpaces.children;
    let activity1 = new Activity("Product Envisioning", scrumPractice);
    let activity2 = new Activity("Release Planning", scrumPractice);
    let activity3 = new Activity("Sprint Plannung", scrumPractice);
    let activity4 = new Activity("Daily Scrum", scrumPractice);
    let activity5 = new Activity("Sprint Review", scrumPractice);
    let activity6 = new Activity("Sprint Retrospective", scrumPractice);
    activity1.concern = concerns[0];
    activity2.concern = concern1;
    activity3.concern = concern1;
    activity4.concern = concerns[2];
    activity5.concern = concern2;
    activity6.concern = concerns[2];
    activity1.activitySpaces = [spaces[0], spaces[1]];
    activity2.activitySpaces = [spaces[4], spaces[10]];
    activity3.activitySpaces = [spaces[4], spaces[11]];
    activity4.activitySpaces = [spaces[12], spaces[13]];
    activity5.activitySpaces = [spaces[7], spaces[2], spaces[13]];
    activity6.activitySpaces = [spaces[13]];
    scrumPractice.ownedElements.activityList.push(activity1);
    scrumPractice.ownedElements.activityList.push(activity2);
    scrumPractice.ownedElements.activityList.push(activity3);
    scrumPractice.ownedElements.activityList.push(activity4);
    scrumPractice.ownedElements.activityList.push(activity5);
    scrumPractice.ownedElements.activityList.push(activity6);
    let alphas = scrumPractice.kernel.ownedElements.alphas.children;
    let workProduct1 = new WorkProduct("Product Vision", scrumPractice);
    let workProduct2 = new WorkProduct("Product Backlog", scrumPractice);
    let workProduct3 = new WorkProduct("Sprint Backlog", scrumPractice);
    let workProduct4 = new WorkProduct("Product Increment", scrumPractice);
    let workProduct5 = new WorkProduct("Scrum Team", scrumPractice);
    let workProduct6 = new WorkProduct("Release Plan", scrumPractice);
    let workProduct7 = new WorkProduct("Sprint Plan", scrumPractice);
    let workProduct8 = new WorkProduct("Task Board", scrumPractice);
    let workProduct9 = new WorkProduct("Work Remaining", scrumPractice);
    let workProduct10 = new WorkProduct("Burndown Chart", scrumPractice);
    let workProduct11 = new WorkProduct("Definition of Done", scrumPractice);
    let workProduct12 = new WorkProduct("Scrum Guide", scrumPractice);
    workProduct1.concern = concerns[0];
    workProduct2.concern = concerns[1];
    workProduct3.concern = concerns[1];
    workProduct4.concern = concerns[1];
    workProduct5.concern = concerns[2];
    workProduct6.concern = concerns[2];
    workProduct7.concern = concerns[2];
    workProduct8.concern = concerns[2];
    workProduct9.concern = concerns[2];
    workProduct10.concern = concerns[2];
    workProduct11.concern = concerns[2];
    workProduct12.concern = concerns[2];
    workProduct1.alphas = [alphas[0], alphas[1]];
    workProduct2.alphas = [alphas[2]];
    workProduct3.alphas = [alphas[2]];
    workProduct4.alphas = [alphas[3]];
    workProduct5.alphas = [alphas[4]];
    workProduct6.alphas = [alphas[5]];
    workProduct7.alphas = [alphas[5]];
    workProduct8.alphas = [alphas[5]];
    workProduct9.alphas = [alphas[5]];
    workProduct10.alphas = [alphas[5]];
    workProduct11.alphas = [alphas[6]];
    workProduct12.alphas = [alphas[6]];
    scrumPractice.ownedElements.workProductList.push(workProduct1);
    scrumPractice.ownedElements.workProductList.push(workProduct2);
    scrumPractice.ownedElements.workProductList.push(workProduct3);
    scrumPractice.ownedElements.workProductList.push(workProduct4);
    scrumPractice.ownedElements.workProductList.push(workProduct5);
    scrumPractice.ownedElements.workProductList.push(workProduct6);
    scrumPractice.ownedElements.workProductList.push(workProduct7);
    scrumPractice.ownedElements.workProductList.push(workProduct8);
    scrumPractice.ownedElements.workProductList.push(workProduct9);
    scrumPractice.ownedElements.workProductList.push(workProduct10);
    scrumPractice.ownedElements.workProductList.push(workProduct11);
    scrumPractice.ownedElements.workProductList.push(workProduct12);
    this.push(scrumPractice);
  }

  @action.bound
  addPractice(name) {
    let practice = new Practice(name, this);
    this.push(practice);
  }
}

class KernelStore extends TreeList {
  constructor(store) {
    super("Kernels");
    this.store = store;
    // TODO: Only for development
    this.addStandardKernel("Kernel 1");
    this.addStandardKernel("Kernel 2");
  }

  @action.bound
  addStandardKernel(name) {
    let kernel = new Kernel(name, this);
    this.push(kernel);
  }
}

const store = new Store();

export default store;
