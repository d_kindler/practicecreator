import React, { Component } from "react";
import { observer } from "mobx-react";
import { remote } from "electron";

import { getContextMenu } from "../utils/TreeExplorerHeaderContextMenu";

const { MenuItem } = remote;

@observer
export default class TreeExplorerHeader extends Component {
  constructor(props) {
    super(props);
    this.contextMenu = this.contextMenu.bind(this);
  }

  contextMenu(e) {
    e.preventDefault();
    const element = this.props.node.element;
    let menu = getContextMenu(element);
    // Append Inspect Element Debug Menu Item
    let clickPos = { x: e.clientX, y: e.clientY };
    menu.append(
      new MenuItem({
        label: "Inspect Element",
        click: () => {
          remote.getCurrentWindow().inspectElement(clickPos.x, clickPos.y);
        }
      })
    );
    menu.popup(e.clientX, e.clientY);
  }

  getAlphaColorRect(node) {
    if (node.element && node.element.concern) {
      return (
        <svg
          width="13"
          height="13"
          style={{
            display: "inline",
            verticalAlign: "middle",
            paddingRight: "5px"
          }}
        >
          <rect
            width="13"
            height="13"
            rx="3"
            ry="3"
            style={{
              fill: node.element.concern.color,
              stroke: "black",
              strokeWidth: "3"
            }}
          />
        </svg>
      );
    }
  }
  render() {
    const node = this.props.node;
    const style = this.props.style;
    return (
      <div
        style={{
          ...style.base,
          borderBottom: " 1px solid black",
          width: "calc(100% - 19px)"
        }}
        onContextMenu={this.contextMenu}
      >
        {this.getAlphaColorRect(node)}
        <div
          style={{
            ...style.title,
            display: "inline",
            whiteSpace: "nowrap",
            textOverflow: "ellipsis"
          }}
        >
          {node.name}
        </div>
      </div>
    );
  }
}
