import React, { Component } from "react";
import { observer, inject } from "mobx-react";
import TreeExplorer from "./TreeExplorer";
import ElementSelector from "./ElementView/ElementSelector";

@inject(store => ({ store }))
@observer
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = { selectedElement: null };
  }

  selectElement = element => {
    this.setState({ selectedElement: element });
  };

  render() {
    let element = null;
    if (this.state.selectedElement && !this.state.selectedElement.deleted) {
      element = this.state.selectedElement;
    }
    return (
      <div className="App">
        <div className="TreeExplorer">
          <TreeExplorer store={this.props.store} selectElement={this.selectElement} />
        </div>
        <div className="KernelDetails">
          <ElementSelector element={element} />
        </div>
      </div>
    );
  }
}
