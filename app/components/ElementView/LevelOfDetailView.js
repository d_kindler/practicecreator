import React, { Component } from "react";
import { observer } from "mobx-react";
import ReactQuill from "react-quill";

@observer
export default class LevelOfDetailView extends Component {
  render() {
    const levelOfDetail = this.props.element;
    return (
      <div>
        <h1>{levelOfDetail.name}</h1>
        <table style={{ width: "100%" }}>
          <tbody>
            <tr>
              <th>name:</th>
              <td>
                <input
                  type="text"
                  value={levelOfDetail.name || ""}
                  onChange={event => levelOfDetail.setName(event.target.value)}
                />
              </td>
            </tr>
            <tr>
              <th>type:</th>
              <td>{levelOfDetail.type}</td>
            </tr>
            <tr>
              <th>description:</th>
              <td>
                <ReactQuill
                  value={levelOfDetail.description || ""}
                  onChange={(value, delta, source) => {
                    if (source === "user") {
                      levelOfDetail.setDescription(value);
                    }
                  }}
                />
              </td>
            </tr>
            <tr>
              <th>is Sufficient Level:</th>
              <td>
                <input
                  type="checkbox"
                  checked={levelOfDetail.isSufficientLevel || false}
                  onChange={event => levelOfDetail.setIsSufficientLevel(event.target.checked)}
                />
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
