import React, { Component } from "react";
import { observer } from "mobx-react";
import Select from "react-select";

import BasicElementView from "./BasicElementView";

@observer
export default class WorkProductView extends Component {
  getMultiList = eList => eList.map(e => ({ value: e.name, label: e.name, realValue: e }));
  render() {
    const workProduct = this.props.element;
    let concerns = null;
    if (workProduct.owner.kernel) {
      concerns = workProduct.owner.kernel.ownedElements.concerns.children;
    }
    return (
      <div>
        <BasicElementView element={workProduct} concerns={concerns} />
        <table style={{ width: "100%" }}>
          <tbody>
            <tr>
              <th>Alphas:</th>
              <td>
                <Select
                  menuPlacement="auto"
                  isMulti="true"
                  value={this.getMultiList(workProduct.alphas)}
                  name="Alphas"
                  options={this.getMultiList(workProduct.allAlphas)}
                  onChange={e => workProduct.setAlphas(e)}
                />
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
