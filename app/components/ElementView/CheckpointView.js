import React, { Component } from "react";
import { observer } from "mobx-react";
import ReactQuill from "react-quill";

@observer
export default class CheckpointView extends Component {
  render() {
    const checkpoint = this.props.element;
    return (
      <div>
        <h3>{checkpoint.name}</h3>
        <table style={{ width: "100%" }}>
          <tbody>
            <tr>
              <th>name:</th>
              <td>
                <input
                  type="text"
                  value={checkpoint.name || ""}
                  onChange={event => checkpoint.setName(event.target.value)}
                />
              </td>
            </tr>
            <tr>
              <th>type:</th>
              <td>{checkpoint.type}</td>
            </tr>
            <tr>
              <th>shortDescription:</th>
              <td>
                <textarea
                  onChange={e => {
                    checkpoint.setShortDescription(e.target.value);
                  }}
                  value={checkpoint.shortDescription || ""}
                  style={{ width: "99%", resize: "vertical" }}
                />
              </td>
            </tr>
            <tr>
              <th>description:</th>
              <td>
                <ReactQuill
                  value={checkpoint.description || ""}
                  onChange={(value, delta, source) => {
                    if (source === "user") {
                      checkpoint.setDescription(value);
                    }
                  }}
                />
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
