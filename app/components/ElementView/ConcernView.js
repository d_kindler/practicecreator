import React, { Component } from "react";
import { observer } from "mobx-react";
import ReactQuill from "react-quill";

@observer
export default class ConcernView extends Component {
  render() {
    const concern = this.props.element;
    return (
      <div>
        <h1>{concern.name}</h1>
        <table style={{ width: "100%" }}>
          <tbody>
            <tr>
              <th>name:</th>
              <td>
                <input
                  type="text"
                  value={concern.name || ""}
                  onChange={event => concern.setName(event.target.value)}
                />
              </td>
            </tr>
            <tr>
              <th>Color:</th>
              <td>
                <input
                  type="color"
                  value={concern.color || "black"}
                  onChange={e => concern.setColor(e.target.value)}
                />
              </td>
            </tr>
            <tr>
              <th>briefDescription:</th>
              <td>
                <textarea
                  onChange={e => concern.setBriefDescription(e.target.value)}
                  value={concern.briefDescription || ""}
                  style={{ width: "99%", resize: "vertical" }}
                />
              </td>
            </tr>
            <tr>
              <th>description:</th>
              <td>
                <ReactQuill
                  value={concern.description}
                  onChange={(value, delta, source) => {
                    if (source === "user") {
                      concern.setDescription(value);
                    }
                  }}
                />
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
