import React, { Component } from "react";
import { observer, inject } from "mobx-react";
import Select from "react-select";
import ReactQuill from "react-quill";

import ElementGroupView from "./ElementGroupView";

@inject(store => ({ kernels: store.kernelStore.children }))
@observer
export default class PracticeView extends Component {
  getKernelList = kernels => kernels.map(k => ({ value: k, label: k.name }));

  render() {
    const practice = this.props.element;
    let kernelTableElement = null;
    if (practice.kernel) {
      kernelTableElement = practice.kernel.name;
    } else {
      kernelTableElement = (
        <Select
          menuPlacement="auto"
          name="Kernel"
          options={this.getKernelList(this.props.kernels)}
          onChange={element => practice.setKernel(element.value)}
        />
      );
    }
    return (
      <div>
        <ElementGroupView element={practice} />
        <table>
          <tbody>
            <tr>
              <th>Kernel:</th>
              <td style={{ minWidth: "200px" }}>{kernelTableElement}</td>
            </tr>
            <tr>
              <th>Consistency Rules:</th>
              <td>
                <ReactQuill
                  value={practice.consistencyRules || ""}
                  onChange={(value, delta, source) => {
                    if (source === "user") {
                      practice.setConsistencyRules(value);
                    }
                  }}
                />
              </td>
            </tr>
            <tr>
              <th>Objective:</th>
              <td>
                <input
                  type="text"
                  value={practice.objective || ""}
                  onChange={event => practice.setObjective(event.target.value)}
                />
              </td>
            </tr>
            <tr>
              <th>Measures:</th>
              <td>
                <ReactQuill
                  value={practice.measures || ""}
                  onChange={(value, delta, source) => {
                    if (source === "user") {
                      practice.setMeasures(value);
                    }
                  }}
                />
              </td>
            </tr>
            <tr>
              <th>Entry:</th>
              <td>
                <ReactQuill
                  value={practice.entry || ""}
                  onChange={(value, delta, source) => {
                    if (source === "user") {
                      practice.setEntry(value);
                    }
                  }}
                />
              </td>
            </tr>
            <tr>
              <th>Result:</th>
              <td>
                <ReactQuill
                  value={practice.result || ""}
                  onChange={(value, delta, source) => {
                    if (source === "user") {
                      practice.setResult(value);
                    }
                  }}
                />
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
