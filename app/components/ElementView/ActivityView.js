import React, { Component } from "react";
import { observer } from "mobx-react";
import Chip from "@material-ui/core/Chip";
import Select from "react-select";

import BasicElementView from "./BasicElementView";

@observer
export default class ActivitySpaceView extends Component {
  getMultiList = eList => eList.map(e => ({ value: e.name, label: e.name, realValue: e }));
  getCLevelList = eList => eList.map(e => ({ value: e.path, label: e.path, realValue: e }));
  render() {
    const activity = this.props.element;
    return (
      <div>
        <BasicElementView
          element={activity}
          concerns={activity.owner.kernel.ownedElements.concerns.children}
        />
        <table style={{ width: "100%" }}>
          <tbody>
            <tr>
              <th>Activity Spaces:</th>
              <td>
                <Select
                  menuPlacement="auto"
                  isMulti="true"
                  name="Activity Spaces"
                  value={this.getMultiList(activity.activitySpaces)}
                  options={this.getMultiList(activity.owner.kernel.ownedElements.activitySpaces.children)}
                  onChange={e => activity.setActivitySpaces(e)}
                />
              </td>
            </tr>
            <tr>
              <th>Required Competency Levels:</th>
              <td>
                <Select
                  menuPlacement="auto"
                  isMulti="true"
                  name="Required Competency Levels"
                  value={this.getCLevelList(activity.requiredCompetencyLevel)}
                  options={this.getCLevelList(activity.allCompetencyLevels)}
                  onChange={e => activity.setRequiredCompetencyLevel(e)}
                />
              </td>
            </tr>
            <tr>
              <th>Criterion:</th>
              <td>
                {activity.criterion.map(criterion => (
                  <Chip
                    key={criterion.id}
                    label={criterion.description}
                    onDelete={() => activity.removeCriterion(criterion)}
                  />
                ))}
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
