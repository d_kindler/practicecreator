import React, { Component } from "react";
import { observer } from "mobx-react";
import ReactQuill from "react-quill";

@observer
export default class StateView extends Component {
  render() {
    const aState = this.props.element;
    return (
      <div>
        <h1>{aState.name}</h1>
        <table style={{ width: "100%" }}>
          <tbody>
            <tr>
              <th>name:</th>
              <td>
                <input
                  type="text"
                  value={aState.name || ""}
                  onChange={event => aState.setName(event.target.value)}
                />
              </td>
            </tr>
            <tr>
              <th>type:</th>
              <td>{aState.type}</td>
            </tr>
            <tr>
              <th>description:</th>
              <td>
                <ReactQuill
                  value={aState.description}
                  onChange={(value, delta, source) => {
                    if (source === "user") {
                      aState.setDescription(value);
                    }
                  }}
                />
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
