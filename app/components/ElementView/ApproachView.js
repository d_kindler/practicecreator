import React, { Component } from "react";
import { observer } from "mobx-react";
import ReactQuill from "react-quill";

@observer
export default class ApproachView extends Component {
  render() {
    const approach = this.props.element;
    return (
      <div>
        <h3>{approach.name}</h3>
        <table style={{ width: "100%" }}>
          <tbody>
            <tr>
              <th>name:</th>
              <td>
                <input
                  type="text"
                  value={approach.name || ""}
                  onChange={event => approach.setName(event.target.value)}
                />
              </td>
            </tr>
            <tr>
              <th>type:</th>
              <td>{approach.type}</td>
            </tr>
            <tr>
              <th>description:</th>
              <td>
                <ReactQuill
                  value={approach.description || ""}
                  onChange={(value, delta, source) => {
                    if (source === "user") {
                      approach.setDescription(value);
                    }
                  }}
                />
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
