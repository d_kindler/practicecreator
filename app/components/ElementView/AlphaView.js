import React, { Component } from "react";
import { observer } from "mobx-react";
import Chip from "@material-ui/core/Chip";

import BasicElementView from "./BasicElementView";

@observer
export default class AlphaView extends Component {
  render() {
    const alpha = this.props.element;
    return (
      <div>
        <BasicElementView element={alpha} concerns={alpha.allConcerns} />
        <table style={{ width: "100%" }}>
          <tbody>
            <tr>
              <th>States:</th>
              <td>
                {alpha.states.map(alphaState => (
                  <Chip
                    key={alphaState.id}
                    label={alphaState.name}
                    onDelete={() => alphaState.destroy()}
                  />
                ))}
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
