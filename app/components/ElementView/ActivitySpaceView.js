import React, { Component } from "react";
import { observer } from "mobx-react";
import Chip from "@material-ui/core/Chip";

import BasicElementView from "./BasicElementView";

@observer
export default class ActivitySpaceView extends Component {
  render() {
    const space = this.props.element;
    return (
      <div>
        <BasicElementView element={space} concerns={space.owner.ownedElements.concerns.children} />
        <table style={{ width: "100%" }}>
          <tbody>
            <tr>
              <th>Criterion:</th>
              <td>
                {space.criterion.map(criterion => (
                  <Chip
                    key={criterion.id}
                    label={criterion.description}
                    onDelete={() => space.removeCriterion(criterion)}
                  />
                ))}
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
