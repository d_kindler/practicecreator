import React, { Component } from "react";
import { observer } from "mobx-react";
import ReactQuill from "react-quill";
import Select from "react-select";

@observer
export default class BasicElementView extends Component {
  getConcernsList = concerns => concerns.map(concern => ({ value: concern, label: concern.name }));
  render() {
    const element = this.props.element;
    let color = "black";
    let defaultConcern = null;
    if (element.concern) {
      color = element.concern.color;
      defaultConcern = { value: element.concern, label: element.concern.name };
    }
    let concernTableElement = null;
    if (this.props.concerns) {
      concernTableElement = (
        <Select
          menuPlacement="auto"
          value={defaultConcern}
          name="Kernel"
          options={this.getConcernsList(this.props.concerns)}
          onChange={e => element.setConcern(e.value)}
        />
      );
    } else {
      concernTableElement = "No Concerns found, check Kernel";
    }
    return (
      <div>
        <h1 style={{ color }}>{element.name}</h1>
        <table style={{ width: "100%" }}>
          <tbody>
            <tr>
              <th>name:</th>
              <td>
                <input
                  type="text"
                  value={element.name || ""}
                  onChange={event => element.setName(event.target.value)}
                />
              </td>
            </tr>
            <tr>
              <th>type:</th>
              <td>{element.type}</td>
            </tr>
            <tr>
              <th>briefDescription:</th>
              <td>
                <textarea
                  onChange={e => element.setBriefDescription(e.target.value)}
                  value={element.briefDescription || ""}
                  style={{ width: "99%", resize: "vertical" }}
                />
              </td>
            </tr>
            <tr>
              <th>description:</th>
              <td>
                <ReactQuill
                  value={element.description || ""}
                  onChange={(value, delta, source) => {
                    if (source === "user") {
                      element.setDescription(value);
                    }
                  }}
                />
              </td>
            </tr>
            <tr>
              <th>concern:</th>
              <td>{concernTableElement}</td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
