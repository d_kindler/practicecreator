import React, { Component } from "react";
import { observer } from "mobx-react";

import PracticeView from "./PracticeView";
import KernelView from "./KernelView";
import ActivitySpaceView from "./ActivitySpaceView";
import ActivityView from "./ActivityView";
import ApproachView from "./ApproachView";
import CriterionView from "./CriterionView";
import CompetencyView from "./CompetencyView";
import CompetencyLevelView from "./CompetencyLevelView";
import ConcernView from "./ConcernView";
import AlphaView from "./AlphaView";
import StateView from "./StateView";
import CheckpointView from "./CheckpointView";
import WorkProductView from "./WorkProductView";
import LevelOfDetailView from "./LevelOfDetailView";

@observer
export default class ElementSelector extends Component {
  render() {
    const element = this.props.element;
    if (element == null) {
      return <div />;
    }
    let kernelComponent = null;
    switch (element.type) {
      case "Practice":
        kernelComponent = <PracticeView element={element} />;
        break;
      case "Activity":
        kernelComponent = <ActivityView element={element} />;
        break;
      case "Approach":
        kernelComponent = <ApproachView element={element} />;
        break;
      case "Kernel":
        kernelComponent = <KernelView element={element} />;
        break;
      case "Activity Space":
        kernelComponent = <ActivitySpaceView element={element} />;
        break;
      case "Criterion":
        kernelComponent = <CriterionView element={element} />;
        break;
      case "Competency":
        kernelComponent = <CompetencyView element={element} />;
        break;
      case "Competency Level":
        kernelComponent = <CompetencyLevelView element={element} />;
        break;
      case "Concern":
        kernelComponent = <ConcernView element={element} />;
        break;
      case "Alpha":
        kernelComponent = <AlphaView element={element} />;
        break;
      case "State":
        kernelComponent = <StateView element={element} />;
        break;
      case "Checkpoint":
        kernelComponent = <CheckpointView element={element} />;
        break;
      case "Work Product":
        kernelComponent = <WorkProductView element={element} />;
        break;
      case "Level Of Detail":
        kernelComponent = <LevelOfDetailView element={element} />;
        break;
      default:
        kernelComponent = <div />;
        break;
    }
    return <div>{kernelComponent}</div>;
  }
}
