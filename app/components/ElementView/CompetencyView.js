import React, { Component } from "react";
import { observer } from "mobx-react";
import Chip from "@material-ui/core/Chip";

import BasicElementView from "./BasicElementView";

@observer
export default class CompetencyView extends Component {
  render() {
    const competency = this.props.element;
    return (
      <div>
        <BasicElementView element={competency} concerns={competency.owner.ownedElements.concerns.children} />
        <table style={{ width: "100%" }}>
          <tbody>
            <tr>
              <th>Level:</th>
              <td>
                {competency.possibleLevel.map(level => (
                  <Chip
                    key={level.id}
                    label={level.name}
                    onDelete={() => level.destroy()}
                  />
                ))}
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
