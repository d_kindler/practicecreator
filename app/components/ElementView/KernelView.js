import React, { Component } from "react";
import { observer } from "mobx-react";
import Chip from "@material-ui/core/Chip";
import ReactQuill from "react-quill";

import ElementGroupView from "./ElementGroupView";

@observer
export default class KernelView extends Component {
  render() {
    const kernel = this.props.element;
    return (
      <div>
        <ElementGroupView element={kernel} />
        <table style={{ width: "100%" }}>
          <tbody>
            <tr>
              <th>Alphas:</th>
              <td>
                {kernel.ownedElements.alphas.children.map(alpha => (
                  <Chip
                    key={alpha.id}
                    label={alpha.name}
                    onDelete={() => kernel.removeAlpha(alpha)}
                  />
                ))}
              </td>
            </tr>
            <tr>
              <th>Consistency Rules:</th>
              <td>
                <ReactQuill
                  value={kernel.consistencyRules || ""}
                  onChange={(value, delta, source) => {
                    if (source === "user") {
                      kernel.setConsistencyRules(value);
                    }
                  }}
                />
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
