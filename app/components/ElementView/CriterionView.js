import React, { Component } from "react";
import { observer, inject } from "mobx-react";
import Select from "react-select";

@observer
export default class CriterionView extends Component {
  render() {
    const criterion = this.props.element;
    return (
      <div>
        <h3>{criterion.name}</h3>
        <table style={{ width: "100%" }}>
          <tbody>
            <tr>
              <th>description:</th>
              <td>
                <input
                  type="text"
                  value={criterion.description || ""}
                  onChange={event => criterion.setDescription(event.target.value)}
                />
              </td>
            </tr>
            <tr>
              <th>type:</th>
              <td>{criterion.type}</td>
            </tr>
            <tr>
              <th>Criterion Type:</th>
              <Select
                menuPlacement="auto"
                name="Criterion Type"
                value={{ value: criterion.criterionType, label: criterion.criterionType }}
                options={[
                  { value: "Completion", label: "Completion" },
                  { value: "Entry", label: "Entry" }
                ]}
                onChange={e => criterion.setCriterionType(e.value)}
              />
            </tr>
            <tr>
              <th>state:</th>
              <td>
                <Select
                  menuPlacement="auto"
                  name="state"
                  value={criterion.state}
                  options={criterion.allStates}
                  getOptionValue={v => v}
                  getOptionLabel={v => v.name}
                  onChange={e => criterion.setState(e)}
                />
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
