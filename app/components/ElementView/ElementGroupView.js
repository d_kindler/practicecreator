import React, { Component } from "react";
import { observer } from "mobx-react";
import ReactQuill from "react-quill";

@observer
export default class ElementGroupView extends Component {
  render() {
    const element = this.props.element;
    return (
      <div>
        <h1>{element.name}</h1>
        <table style={{ width: "100%" }}>
          <tbody>
            <tr>
              <th>name:</th>
              <td>
                <input
                  type="text"
                  value={element.name || ""}
                  onChange={event => element.setName(event.target.value)}
                />
              </td>
            </tr>
            <tr>
              <th>type:</th>
              <td>{element.type}</td>
            </tr>
            <tr>
              <th>briefDescription:</th>
              <td>
                <textarea
                  onChange={e => {
                    element.setBriefDescription(e.target.value);
                  }}
                  value={element.briefDescription || ""}
                  style={{ width: "99%", resize: "vertical" }}
                />
              </td>
            </tr>
            <tr>
              <th>description:</th>
              <td>
                <ReactQuill
                  value={element.description || ""}
                  onChange={(value, delta, source) => {
                    if (source === "user") {
                      element.setDescription(value);
                    }
                  }}
                />
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
