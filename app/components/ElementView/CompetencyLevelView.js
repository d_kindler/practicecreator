import React, { Component } from "react";
import { observer } from "mobx-react";

@observer
export default class CompetencyLevelView extends Component {
  render() {
    const level = this.props.element;
    return (
      <div>
        <h1>{level.name}</h1>
        <table style={{ width: "100%" }}>
          <tbody>
            <tr>
              <th>name:</th>
              <td>
                <input
                  type="text"
                  value={level.name || ""}
                  onChange={event => level.setName(event.target.value)}
                />
              </td>
            </tr>
            <tr>
              <th>briefDescription:</th>
              <td>
                <textarea
                  onChange={e => level.setBriefDescription(e.target.value)}
                  value={level.briefDescription || ""}
                  style={{ width: "99%", resize: "vertical" }}
                />
              </td>
            </tr>
            <tr>
              <th>Level:</th>
              <td>
                <input
                  type="number"
                  value={level.level || 0}
                  style={{ width: "5em" }}
                  onChange={e => level.setLevel(e.target.value)}
                />
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
