const modal = require("electron-modal");

document.getElementById("name").addEventListener("keyup", (e) => {
  if (e.keyCode === 13) {
    // Trigger the button element with a click
    document.getElementById("save").click();
  }
});

document.getElementById("save").addEventListener("click", () => {
  modal.emit("save", document.getElementById("name").value).then(() => {
    modal.hide();
  });
});

document.getElementById("close").addEventListener("click", () => {
  modal.hide();
});

modal.getData().then(data => {
  // Apply the data you passed to the modal
  document.querySelector("title").innerHTML = data.title;

  // And once we're ready, let's show it!
  modal.show();
});
