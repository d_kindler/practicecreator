import React, { Component } from "react";
import { observer } from "mobx-react";
import { Treebeard, decorators } from "react-treebeard";

import { generateTree } from "../utils/TreeViewGenerator";
import TreeExplorerHeader from "./TreeExplorerHeader";

decorators.Header = ({ style, node }) => <TreeExplorerHeader style={style} node={node} />;

@observer
export default class TreeExplorer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.onToggle = this.onToggle.bind(this);
  }

  onToggle(node, toggled) {
    if (this.state.cursor) {
      this.state.cursor.active = false;
      this.state.cursor.element.setActive(false);
    }
    node.active = true; // eslint-disable-line no-param-reassign
    node.element.setActive(true);
    if (node.children) {
      node.toggled = toggled; // eslint-disable-line no-param-reassign
      node.element.setToggled(toggled);
    }
    this.setState({ cursor: node });
    this.props.selectElement(node.element);
  }

  generateTree(store) {
    return generateTree(store);
  }

  render() {
    return (
      <div id="TreeExplorer">
        <Treebeard
          data={this.generateTree(this.props.store)}
          onToggle={this.onToggle}
          decorators={decorators}
        />
      </div>
    );
  }
}
